import {useEffect, useState} from "react";
import {WordItem} from "../wordItem";
import {Text, View} from "@tarojs/components";


interface SentenceType  {
  sentence: string,
  setWord:any,
  showWordInfo:any

}
export const WordSplit=(props: SentenceType)=>{
  const [words,setWords] = useState<string[]>([]);

  useEffect(()=>{
    // setWords(props.sentence.split(" "))
    splitSentence(props.sentence);
  },[props.sentence])

  const isChar = (char:string)=>{
    if ((char >= 'A' && char <= 'Z') || (char >= 'a' && char <= 'z') ){
      return true;
    }
    return false;
  }

  const haveSymbol = (word:string)=>{
    //分割的单词是否有标点
    let chars = word.split('')
    for(let i = 0;i<chars.length;i++){
      if (!isChar(chars[i])){
        return true;
      }
    }
    return false;
  }

  const splitWord_haveSymbol = (word:string)=>{
    let newWords: string[] = [];
    // if (!haveSymbol(word)){
    //   return undefined;
    // }
    let chars = word.split('')
    let w = '';
    for (let i = 0; i < chars.length; i++){
      if (!isChar(chars[i])){
        if (w.length>0){
          newWords.push(w)
        }
        w='';

        newWords.push(chars[i])
      }
      else {
        w+=chars[i];
      }
    }
    if (w.length>0){
      newWords.push(w)
    }
    console.log(newWords)
    return newWords;
  }

  const splitSentence = (sentence:string)=>{
    let wordList = sentence.split(" ")
    let result : string[] = [];
    for (let i = 0;i < wordList.length;i++){
      if (haveSymbol(wordList[i])){
        splitWord_haveSymbol(wordList[i]).map((item)=>{
          result.push(item)
        })
      }else {
        result.push(wordList[i])
      }
    }
    //   let chars = wordList[i].split('')
    //   for(let ci = 0;ci<chars.length;ci++){
    //     if (!isChar(chars[ci])){
    //       console.log('c',chars[ci])
    //     }
    //   }
    // }
    console.log(result)
    setWords(result);
  }

  return <>
    <View className='words-split-body'>
      {words.map((item)=>{
        if (!haveSymbol(item)){
          return <>
            <WordItem word={item} showWordInfo={props.showWordInfo} setWord={props.setWord}></WordItem>
          </>
        }
        else {
          return <>
            <Text >{item}</Text>
          </>
        }
      })}
    </View>
  </>
}

