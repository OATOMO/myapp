import { Text, View} from "@tarojs/components";
import {AtIcon } from 'taro-ui';
import {getWordSound_UK, getWordSound_US} from "../../unils/myrequest";
import './index.scss'

interface WordSoundProps{
  word:string,
}

export const WordSound=({word}:WordSoundProps)=>{
  return <View>
    <View className='soundItem'>
      <Text>英</Text>
      <AtIcon className='soundIcon' value='volume-plus' onClick={()=>getWordSound_UK(word)}></AtIcon>
    </View>

    <View className='soundItem'>
      <Text>美</Text>
      <AtIcon className='soundIcon' value='volume-plus' onClick={()=>getWordSound_US(word)}></AtIcon>
    </View>
  </View>
}
