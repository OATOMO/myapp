import {useEffect, useState} from "react";
import {Text,View} from "@tarojs/components";
import {getWordCN} from "../../unils/myrequest";
import './index.scss'
import {WordSound} from "../wordSound";

export type WordInfoQueryType = {
  word: string,
  visible: boolean,
  cancelHandle: any,
}
export const WordInfo = (props: WordInfoQueryType) =>{
  const [data,setdata] = useState({});
  const visible = props.visible;

  useEffect(()=>{
    getWordCN(props.word, setdata)
  },[props.word])
  const oncancel = ()=>{
    props.cancelHandle();
  }
  const status = data
  const words_cn = data['words_cn']
  // console.log(data,status,words_cn)
  console.log(data)
  if (!visible){
    return null;
  }
  return <View>
    <View className='modal-mask' onClick={oncancel}></View>
      <View className='modal-container'>
        <View className='modal-header'>
          <View className='modal-title'>
            {props.word}
          </View>
          <View className='modal-body'>
            <WordSound word={props.word}></WordSound>
            {(status && words_cn)? words_cn.map((item)=>{
              return <Text className='words-explain-item'>
                {item.explain}
              </Text>
            }):null }
          </View>
          <View className='modal-footer'>
            {/*<AtButton className='modal-cancel-btn'></AtButton>*/}
            {/*<AtButton className='modal-confirm-btn'></AtButton>*/}
          </View>
        </View>
      </View>
  </View>


}
