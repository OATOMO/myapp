import {Text, View} from "@tarojs/components";
import './index.scss'

interface PropsType{
  word:string,
  setWord:any,
  showWordInfo:any
}
export const WordItem = (props:PropsType)=>{

  const handClick = ()=>{
    props.setWord(props.word);
    props.showWordInfo();
  }

  return <>
    <View className='wordItem' onClick={handClick}>
      <Text>{props.word}</Text>
    </View>
  </>
}
