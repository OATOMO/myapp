/**
    首页公共的模块标题
 */
import { View } from '@tarojs/components';
import './index.scss';

interface AreaTitleProps {
    title: string;
    isDetail?: boolean; // 是否是列表详情
}

export const AreaTitle = ({ title, isDetail }: AreaTitleProps) => {
    return  <View className={`area-title ${isDetail ? 'area-title-detail' : ''}`}>
        <View className='`area-title__vertical-line'></View>
        <View className='area-title__text'>{title}</View>
    </View>
}
