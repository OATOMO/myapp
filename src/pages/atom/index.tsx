import {Text, View} from "@tarojs/components";
import {useState} from "react";
import Taro from '@tarojs/taro';
import {AtFab} from "taro-ui";
import {WordInfo} from "../components/wordInfo";
import {WordSplit} from "../components/wordSplit";
import './index.scss'
import {AreaTitle} from "../components/AreaTitle";
import {getBoi} from "../unils/myrequest";

const get_MEveryDayWords = ()=>{
  return 'A phrase about looking far.A phrase about looking far.'
}


const Atom: Taro.FC = () =>{
  const [visible,setVisible] = useState(false);
  const [word,setWord] = useState('');
  const [everyDayWords,setEveryDayWords] = useState(get_MEveryDayWords);
  const cancelHandle = ()=>{
    setVisible(false)
  }

  const showWordInfo = ()=>{
    setVisible(true);
  }

  const changeEveryDayWords = ()=>{
    getBoi(setEveryDayWords);
  }

  return(
    <>
      <view className='index'>

        {/*浮动btn*/}
        <View className='AtFab-btn'>
          <AtFab size='small' onClick={changeEveryDayWords}>
            <Text className='at-fab__icon at-icon at-icon-menu'></Text>
          </AtFab>
        </View>

        {/*区域标题*/}
        <AreaTitle title='every word'></AreaTitle>
        {/*词块组*/}
        <WordSplit sentence={everyDayWords} setWord={setWord} showWordInfo={showWordInfo}></WordSplit>
        {/*翻译modal*/}
        <WordInfo word={word} visible={visible} cancelHandle={cancelHandle}></WordInfo>
      </view>
    </>
  );
}

export default Atom
