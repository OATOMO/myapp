import Taro from "@tarojs/taro";
import * as querystring from "querystring";

export const getWordCN = (work: string, setData) => {
  // var result = new Object();
  // result['status'] = false;
  Taro.request({
    url: 'https://dict.youdao.com/suggest', //仅为示例，并非真实的接口地址
    data: {
      'n': 5,
      'ver': 3.0,
      'doctype': 'json',
      'cache': 'false',
      'le': 'en',
      'q': work
    },
    header: {
      'content-type': 'application/json' // 默认值
    },
    method: 'GET',
    success: function (res) {
      setData({'word':work,'words_cn':res.data.data.entries,'status':true})
    },
    fail: function (res) {
      console.error(res)
    }
  })
}

export const getWordSound_US = (work: string)=>{getWordSound(work, 0)};
export const getWordSound_UK = (work: string)=>{getWordSound(work, 1)};

export const getWordSound = (work: string, type: number) => {
  const result = new Object();
  result['status'] = false;
  if (type != 1 && type != 0){
    return result;
  }
  const innerAudioContext = Taro.createInnerAudioContext()
  innerAudioContext.autoplay = true
  innerAudioContext.src = 'https://dict.youdao.com/dictvoice?' + querystring.stringify({
    'type':type,
    'audio':work
  })
  innerAudioContext.onPlay(() => {
    console.log('开始播放')
  })
  innerAudioContext.onError((res) => {
    console.log(res.errMsg)
    console.log(res.errCode)
  })
  // innerAudioContext.onEnded(() => {
  //   console.log('播放完了')
  // })
}

export const getBoi = (setData) => {
  // result['status'] = false;
  Taro.request({
    url: 'https://m687195m77.zicp.fun/data/boi', //仅为示例，并非真实的接口地址
    data: {
    },
    header: {
      'content-type': 'application/json' // 默认值
    },
    method: 'GET',
    success: function (res) {
      setData(res.data)
      // console.log(res)
    },
    fail: function (res) {
      console.error(res)
    }
  })
}
