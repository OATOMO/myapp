import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import {AtButton} from "taro-ui";
import Taro from "@tarojs/taro";
import {ActItem} from "../components/actItem";

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <Text>Hello world!</Text>
        <AtButton onClick={
          ()=>{Taro.navigateTo({url: '/pages/atom/index'})}
        }
        >boi</AtButton>
        <ActItem></ActItem>
      </View>
    )
  }
}
